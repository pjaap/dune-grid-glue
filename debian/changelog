dune-grid-glue (2.8~20210831-1) experimental; urgency=medium

  * New upstream snapshot.
  * d/patches: Removed obsolet patches

 -- Patrick Jaap <patrick.jaap@tu-dresden.de>  Mon, 24 Aug 2021 12:29:24 -0400

dune-grid-glue (2.7.0-3) unstable; urgency=medium

  * Use tag that is guaranteed to be valid. (Closes: #966965)
    + new patch: 0001-use-0-as-tag-in-MPI-to-avoid-too-large-value.patch

 -- Ansgar <ansgar@debian.org>  Sat, 14 Nov 2020 12:29:58 +0100

dune-grid-glue (2.7.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Ansgar <ansgar@debian.org>  Wed, 15 Jul 2020 12:43:08 +0200

dune-grid-glue (2.7.0-1) experimental; urgency=medium

  * New upstream release.
  * Drop obsolete patch.
    - removed patch: increase-epsilon.patch
  * Use debhelper compat level 13.
  * Bumped Standards-Version to 4.5.0 (no changes).

 -- Ansgar <ansgar@debian.org>  Wed, 10 Jun 2020 00:09:33 +0200

dune-grid-glue (2.6~20180130-1) unstable; urgency=medium

  * New upstream snapshot.
  * Bumped Standards-Version to 4.1.4 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 10 Apr 2018 10:51:19 +0200

dune-grid-glue (2.6~20171108-1) experimental; urgency=medium

  * New upstream snapshot (commit: dee66b1e106417199488c1231d5eb15b5ed4ff8b).
  * d/control: add `Rules-Requires-Root: no`
  * libdune-grid-glue-doc: add Built-Using: doxygen (for jquery.js)
  * Bumped Standards-Version to 4.1.1 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 21 Nov 2017 11:06:25 +0100

dune-grid-glue (2.5.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright: Update for new upstream release.
  * Bumped Standards-Version to 4.1.0 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 29 Aug 2017 20:38:26 +0200

dune-grid-glue (2.5.0~20170717g2795723-1) unstable; urgency=medium

  * New upstream snapshot.
  * No longer build manual -dbg package. Use the automatically generated
    -dbgsym package instead.
  * debian/copyright: Update URLs.
  * Bumped Standards-Version to 4.0.0.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 18 Jul 2017 15:26:22 +0200

dune-grid-glue (2.5.0~20161206g666200e-2) unstable; urgency=medium

  * Increase epsilon to make test pass on i386.
    + new patch: increase-epsilon.patch

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 07 Dec 2016 22:20:27 +0100

dune-grid-glue (2.5.0~20161206g666200e-1) unstable; urgency=medium

  * New upstream snapshot.
  * Switch to CMake.
  * Bumped Standards-Version to 3.9.8 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 06 Dec 2016 19:47:17 +0100

dune-grid-glue (2.4.0-1) unstable; urgency=medium

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 21 Jan 2016 10:54:35 +0100

dune-grid-glue (2.4~20150723gd6e4c74-2) unstable; urgency=medium

  * Upload to unstable.
  * (Build-)Depend on last DUNE upload.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 13 Sep 2015 14:23:55 +0200

dune-grid-glue (2.4~20150723gd6e4c74-1) experimental; urgency=medium

  * New upstream snapshot.
  * Move shared library into -dev package and provide a virtual package
    that changes with the upstream version for shlib dependencies. See
    also https://lists.debian.org/debian-devel/2015/07/msg00115.html
  * libdune-grid-glue-dev: Add -doc package as a suggested package.
  * Add Build-Depends-Indep: graphviz for "dot".
  * Enable tests, except for two broken ones.
  * Bumped Standards-Version to 3.9.6 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 07 Sep 2015 15:23:03 +0200

dune-grid-glue (2.3.1-1) unstable; urgency=low

  * Initial release. (Closes: #756390)

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 08 Oct 2014 20:27:05 +0200
